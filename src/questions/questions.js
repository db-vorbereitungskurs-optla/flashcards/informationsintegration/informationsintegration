export const questions = [
  [
    'Definieren Sie den Begriff Informationsintegration und beleuchten Sie insbesondere die Rolle der Heterogenität bei der Entwicklung komplexer Informationssysteme.',
    'Informationsintegration bezieht sich auf den Prozess, verschiedene Datenquellen oder -formate zu vereinen, um ein einheitliches, konsistentes Informationssystem zu schaffen. Die Integration heterogener Daten, u.a. bedingt durch unterschiedliche Quellen, Schemata oder Formate, erfordert Strategien wie Mapping und Transformation, um Daten zu harmonisieren, die Interoperabilität von Daten in komplexen Systemen zu gewährleisten und auf diese Weise verteilte Informationen effektiv nutzen zu können.',
  ],
  [
    'Unterscheiden Sie die Begriffe strukturelle und semantische Heterogenität. Erläutern Sie zentrale Ursachen ihres Auftretens.',
    'Strukturelle Heterogenität tritt auf, wenn die Schemata zweier Datenmodelle den gleichen Realweltausschnitt erfassen, sich jedoch im Aufbau voneinander unterscheiden. Semantische Heterogenität entsteht demgegenüber, wenn zwei Datenmodelle übereinstimmende Konzepte mit unterschiedlichen Begriffen abbilden. In der Praxis kann Heterogenität durch verschiedene Faktoren hervorgerufen werden. Hierzu zählen beispielsweise unterschiedliche Datenquellen und -modelle, Anwendungsdomänen und historische Entwicklungen in der Datenverwaltung.',
  ],
  [
    'Welche Konsequenzen ergeben sich aus dem Vorhandensein struktureller und semantischer Heterogenitäten? Beschreiben Sie zentrale Strategien für den Umgang mit diesen Arten von Heterogenität.',
    'Strukturelle Heterogenitäten können aufgrund der notwendigen Zusammenführung verschiedener Datenstrukturen und -formate zu einer erhöhten Komplexität der Datenverarbeitung und verschiedenen Interoperabilitätsproblemen führen. Semantische Heterogenitäten verschiedener Datenmodelle können demgegenüber vor allem Verständnis- und Kommunikationsprobleme verursachen. Bestehenden strukturellen Heterogenitäten kann durch verschiedene Methoden der Datenintegration zur Datenbereinigung, -transformation und -harmonisierung, sowie durch den Einsatz von Softwarelösungen zur Vermittlung zwischen den verschiedenen Datenquellen begegnet werden. Die Auflösung semantischer Heterogenitäten erfordert eine klare Definition und Standardisierung von Begriffen und Strukturen der Datenmodellierung.',
  ],
  [
    'Unterscheiden Sie die Begriffe materialisierte und virtuelle Informationsintegration. Nennen Sie jeweils Beispiele für entsprechende Integrationsarchitekturen.',
    'Materialisierte Informationsintegration zentralisiert Daten physisch an einem Speicherort, beispielsweise in Data Warehouses. Im Gegensatz dazu integriert virtuelle Informationsintegration Daten aus verschiedenen Quellen dynamisch und virtuell, etwa in Form von föderierten Datenbanksystemen oder Peer-Daten-Management-Systemen',
  ],
  [
    'Erläutern Sie, welche Ziele Schema Mapping und Schema Matching verfolgen.',
    'Schema Matching zielt darauf ab, Gemeinsamkeiten und Beziehungen zwischen den Strukturen verschiedener Datenschemata automatisch zu identifizieren. Im Zuge des Schema Mapping wird festgelegt, wie Daten zwischen diesen identifizierten Strukturen transformiert und integriert werden sollen, um eine kohärente Datenbasis zu schaffen.',
  ],
]
