function NavigationButtons({
  maxQuestionIndex,
  navigationState,
  navigationCallbackFunction,
}) {
  return (
    <div
      className="btn-group-vertical"
      role="group"
      aria-label="Vertical button group"
    >
      <button
        type="button"
        className="btn btn-outline-primary"
        onClick={() => {
          if (navigationState > 0)
            navigationCallbackFunction(navigationState - 1)
        }}
      >
        <i className="bi bi-arrow-up"></i>
      </button>
      <button
        type="button"
        className="btn btn-outline-primary"
        onClick={() => {
          if (navigationState < maxQuestionIndex)
            navigationCallbackFunction(navigationState + 1)
        }}
      >
        <i className="bi bi-arrow-down"></i>
      </button>
    </div>
  )
}

export default NavigationButtons
