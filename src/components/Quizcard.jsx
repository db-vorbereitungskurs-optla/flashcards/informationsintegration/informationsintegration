import { useState } from 'react'

function Quizcard({
  questionNumber,
  questionsTotal,
  questionText,
  answerText,
}) {
  const [answerVisible, setAnswerVisible] = useState(false)

  return (
    <>
      {!answerVisible ? (
        <div className="card w-100">
          <div className="card-header">
            <span>
              <b>
                Frage {questionNumber} von {questionsTotal}
              </b>
            </span>
          </div>
          <div className="card-body">
            <p className="card-text" style={{ hyphens: 'auto' }}>
              {questionText}
            </p>
            <button
              className="btn btn-primary"
              type="button"
              onClick={() => {
                answerVisible ? setAnswerVisible(false) : setAnswerVisible(true)
              }}
            >
              Lösung anzeigen
            </button>
          </div>
        </div>
      ) : (
        <div className="card w-100">
          <div className="card-header d-flex justify-content-between">
            <span>
              <b>Antwort</b> – Frage {questionNumber}
            </span>
            <button
              type="button"
              className="btn-close"
              aria-label="Close"
              onClick={() => setAnswerVisible(false)}
            ></button>
          </div>
          <div className="card-body">
            <p className="card-text" style={{ hyphens: 'auto' }}>
              {answerText}
            </p>
          </div>
        </div>
      )}
    </>
  )
}

export default Quizcard
